<?php
require __DIR__ . "/../vendor/autoload.php";
use \Maksoft\Form\Validators\FileTypeMatch;


class TestForm extends \Maksoft\Form\Forms\DivForm
{
	public function __construct($form_data=null)
	{
			$this->name = \Maksoft\Form\Fields\TextField::init(array(
					"name" => "name",
					"class" => "asdasd",
					"id" => "23339213012",
			));

			$this->SiteID = \Maksoft\Form\Fields\TextField::init(array(
					"name" => "SiteID",
					"hidden" => True,
			));
            $a = Maksoft\Form\Validators\Integerish::init();
			$this->SiteID->add_validators(Maksoft\Form\Validators\Integerish::init());
			parent::__construct($form_data);
	}
}


class TestFormEmail extends \Maksoft\Form\Forms\DivForm
{
	public function __construct($form_data=null)
	{
			$this->email = new \Maksoft\Form\Fields\TextField(array(
					"class" => "asdasd",
					"id" => "23339213012",
			));

			$this->SiteID = new \Maksoft\Form\Fields\TextField(array(
					"name" => "SiteID",
					"hidden" => True,
			));
			$this->SiteID->add_validators(Maksoft\Form\Validators\Integerish::init());
                
			parent::__construct($form_data);
	}
}


class Input extends \Maksoft\Form\Forms\DivForm
{
    public function __construct($form_data)
    {
         $this->csv = new \Maksoft\Form\Fields\FileInputField(array(
             "name"     => "csv",
             "id"       => "csv",
             "label"    => "Drop csv here",
             "class"    => "form-control",
             "required" => True ));
 
         $this->images = new \Maksoft\Form\Fields\FileInputField(array(
             "label"    => "DropImages Here",
             "name"     => "images[]",
             "id"       => "img",
             "class"    => "form-control",
             "multiple" => True,
             ));
         $this->csv->add_validators(new Maksoft\Form\Validators\FileTypeMatch("text/csv"));
         $this->images->add_validators(\Maksoft\Form\Validators\NotBiggerThan::init(4194304)); // 4MB
         $this->images->add_validators(new FileTypeMatch("image/jpeg"));
         $this->images->add_validators(\Maksoft\Form\Validators\FileExtensionMatch::init("jpg", "JPG", ".jpg"));

         $this->submit = new \Maksoft\Form\Fields\SubmitButton(array("class"=>"btn btn-default"));
         parent::__construct($form_data);
    }
}


class Validators extends PHPUnit_Framework_TestCase
{
    public function setUp()
    {
        $_SESSION = array();

        $this->password = new Maksoft\Form\Fields\PasswordField(["label"=>'Парола', "class"=>'form-control']);
        $this->password->add_validators(new Maksoft\Form\Validators\MaxLength(8));
        $this->password->add_validators(new Maksoft\Form\Validators\MinLength(5));
        $this->password->add_validators(new Maksoft\Form\Validators\HasDigit());
        $this->password->add_validators(new Maksoft\Form\Validators\HasUpperCase());
        $this->special_chars = "?!@#$%^&(*)";
        $this->file = array(
            "name" => "img1.jpg",
            "size" => 512,
            "type" => "image/jpeg"
        );

        $_FILES = array (
            'csv' => array(
                'name' => 'new_import_sheet.csv',
                'type' => 'text/csv',
                'tmp_name' => '/tmp/phpgnWjew',
                'error' => 0, 'size' => 3091,
                ),
            'images' => array (
                'name' => array ( 0 => '354.jpg', 1 => '34914.jpg', 2 => '42365.jpg',),
                'type' => array ( 0 => 'image/jpeg', 1 => 'image/jpeg', 2 => 'image/jpeg',),
                'tmp_name' => array ( 0 => '/tmp/phpUCE5Z9', 1 => '/tmp/phpSiM4w6', 2 => '/tmp/php4ylTk3',),
                'error' => array ( 0 => 0, 1 => 0, 2 => 0,),
                'size' => array ( 0 => 267355, 1 => 51991, 2 => 273041,),)
            );
    }

    /**
     * @covers Maksoft\Form\Validators\BiggerThan::__invoke
     */
    public function test_not_bigger_than()
    {
        $validator = new Maksoft\Form\Validators\BiggerThan(1024);
        $this->assertFalse($validator($this->file));
    }

    public function test_add_images()
    {
        $input = new Input($_POST);

        $_FILES = array (
            'csv' => array(
                'name' => 'new_import_sheet.csv',
                'type' => 'text/csv',
                'tmp_name' => '/tmp/phpgnWjew',
                'error' => 0, 'size' => 3091,
                ),
            'images' => array (
                'name' => array ( 0 => '354.jpg', 1 => '34914.jpg', 2 => '42365.jpg',),
                'type' => array ( 0 => 'image/jpeg', 1 => 'image/jpeg', 2 => 'image/jpeg',),
                'tmp_name' => array ( 0 => '/tmp/phpUCE5Z9', 1 => '/tmp/phpSiM4w6', 2 => '/tmp/php4ylTk3',),
                'error' => array ( 0 => 0, 1 => 0, 2 => 0,),
                'size' => array ( 0 => 267355, 1 => 51991, 2 => 273041,),)
            );
        #$input->is_valid();
    }

    /**
     * @covers Maksoft\Form\Validators\BiggerThan::__invoke
     */
    public function test_bigger_than()
    {
        $this->file['size'] = 1512;
        $validator = new Maksoft\Form\Validators\BiggerThan(1024);
        $this->assertTrue($validator($this->file));
    }

    /**
     * @covers Maksoft\Form\Validators\NotBiggerThan::__invoke
     */
    public function test_smaller_than_given_value()
    {
		$this->file['size'] = 200;
        $validator = new Maksoft\Form\Validators\NotBiggerThan(1024);
        $this->assertTrue($validator($this->file));
    }

    public function test_NOTsmaller_than_given_value()
    {
        $this->file['size'] = 212300;
        $validator = new Maksoft\Form\Validators\NotBiggerThan(1024);
        $this->assertFalse($validator($this->file));
    }

    /**
     * @covers Maksoft\Form\Validators\BiggerThan::__invoke
     */
    public function test_bigger_than_no_value()
    {
        $validator = new Maksoft\Form\Validators\BiggerThan(1024);
        $this->assertFalse($validator());
    }

    /**
     * @covers Maksoft\Form\Validators\FileTypeMatch::__invoke
     */
    public function test_file_match()
    {
        $validator = new Maksoft\Form\Validators\FileTypeMatch("image/jpeg");
        $this->assertTrue($validator($this->file));
    }

    public function test_file_extension_assert_False()
    {
		$validator = new Maksoft\Form\Validators\FileExtensionMatch(".JPEG");
		$this->assertFalse($validator($this->file)); //file extension is jpg
	}

    public function test_file_extension_single_input_assert_True()
    {
		$validator = new Maksoft\Form\Validators\FileExtensionMatch(".jpg");
		$this->assertTrue($validator($this->file)); //file extension is jpg
	}

	public function test_file_extenstion_multiple_assert_True()
	{
		$validator = new Maksoft\Form\Validators\FileExtensionMatch("JPEG", "png", "jpeg", "gif");
		$this->assertFalse($validator($this->file)); //file extension is jpg
	}

	public function test_file_extenstion_multiple_assert_False()
	{
		$validator = new Maksoft\Form\Validators\FileExtensionMatch("JPEG", "png", "swf", "gif");
		$this->assertFalse($validator($this->file)); //file extension is jpg
	}

    /**
     * @covers Maksoft\Form\Validators\FileTypeMatch::__invoke
     */
    public function test_file_not_match()
    {
        $this->file['type'] = "image/png";
        $validator = new Maksoft\Form\Validators\FileTypeMatch("image/jpeg");
        $this->assertFalse($validator($this->file));
    }

    /**
     * @covers Maksoft\Form\Validators\FileTypeMatch::__invoke
     */
    public function test_file_not_match_no_value_provided()
    {
        $validator = new Maksoft\Form\Validators\FileTypeMatch("image/jpeg");
        $this->assertFalse($validator());
    }

    /**
     * @covers Maksoft\Form\Validators\NotEmpty::__invoke
     */
    public function test_not_empty()
    {
        $validator = new Maksoft\Form\Validators\NotEmpty(True);
        $this->assertTrue($validator("asdasda"));
    }

    /**
     * @covers Maksoft\Form\Validators\NotEmpty::__invoke
     */
    public function test_is_empty()
    {
        $validator = new Maksoft\Form\Validators\NotEmpty(False);
        $this->assertTrue($validator());
    }

    /**
     * @covers Maksoft\Form\Validators\HasDigit::__invoke
     */
    public function test_has_digit()
    {
        $validator = new Maksoft\Form\Validators\HasDigit();
        $this->assertTrue($validator("asdasd1"));
    }
    /**
     * @covers Maksoft\Form\Validators\HasDigit::__invoke
     */
    public function test_hasnt_digit()
    {
        $validator = new Maksoft\Form\Validators\HasDigit();
        $this->assertFalse($validator("asdasd"));
    }

    /**
     * @covers Maksoft\Form\Validators\HasSpecialChar::__invoke
     */
    public function test_has_special_char()
    {
        $validator = new Maksoft\Form\Validators\HasSpecialChars($this->special_chars);
        $this->assertTrue($validator("jaskdhaskjdhksaj%"));
    }

    /**
     * @covers Maksoft\Form\Validators\HasSpecialChar::__invoke
     */
    public function test_hasn_special_char()
    {
        $validator = new Maksoft\Form\Validators\HasSpecialChars($this->special_chars);
        $this->assertFalse($validator("jaskdhaskjdhksaj"));
    }

    /**
     * @covers Maksoft\Form\Validators\HasUpperCase::__invoke
     */
    public function test_has_upper_case_letter()
    {
        $validator = new Maksoft\Form\Validators\HasUpperCase();
        $this->assertTrue($validator("jaskdhaskjdhksaAAA"));
    }

    /**
     * @covers Maksoft\Form\Validators\HasUpperCase::__invoke
     */
    public function test_hasnt_upper_case_letter()
    {
        $validator = new Maksoft\Form\Validators\HasUpperCase();
        $this->assertFalse($validator("jaskdhaskjdhksaj"));
    }

    /**
     * @covers Maksoft\Form\Validators\MaxLength::__invoke
     */
    public function test_max_length_true()
    {
        $validator = new Maksoft\Form\Validators\MaxLength(10);
        $this->assertTrue($validator("asdfgh"));
    }

    /**
     * @covers Maksoft\Form\Validators\MaxLength::__invoke
     */
    public function test_max_length_false()
    {
        $validator = new Maksoft\Form\Validators\MaxLength(5);
        $this->assertFalse($validator("jaskdhaskjdhksaj"));
    }

    /**
     * @covers Maksoft\Form\Validators\MinLength::__invoke
     */
    public function test_min_length_true()
    {
        $validator = new Maksoft\Form\Validators\MinLength(10);
        $this->assertTrue($validator("asdfghsasasdasdasdas"));
    }
    /**
     * @covers Maksoft\Form\Validators\MinLength::__invoke
     */
    public function test_min_length_false()
    {
        $validator = new Maksoft\Form\Validators\MinLength(5);
        $this->assertFalse($validator("asd"));
    }

    /**
     * @covers Maksoft\Form\Fields\FileInputField::is_valid
     */
    public function test_file_input_field_one_file_csv_true()
    {
        $csv = new Maksoft\Form\Fields\FileInputField(array(
              "name" => "csv",
              "id"   => "csv",
              "label"=>"Drop csv here",
              "class"=>'form-control',
              "required"=>True ));

        $csv->add_validators(new Maksoft\Form\Validators\FileTypeMatch("text/csv"));
        $this->assertTrue($csv->is_valid());
    }

    /**
     * @covers Maksoft\Form\Fields\FileInputField::is_valid
     */
    public function test_file_input_field_one_file_not_csv_return_exception()
    {
        $csv = new Maksoft\Form\Fields\FileInputField(array(
              "name" => "csv",
              "id"   => "csv",
              "label"=>"Drop csv here",
              "class"=>'form-control',
              "required"=>True ));
        $_FILES['csv']['type'] = "image/jpeg";
        $validator = new Maksoft\Form\Validators\FileTypeMatch("text/csv");
        $csv->add_validators($validator);
        try {
            $csv->is_valid();
        } catch (Exception $e) {
            $this->assertEquals(Maksoft\Form\Fields\BaseField::VALIDATOR_FAIL, $e->getCode());
            $this->assertEquals($validator->msg, $e->getMessage());
        }
    }

    /**
     * @covers Maksoft\Form\Fields\FileInputField::is_valid
     */
    public function test_multiple_files_input_filetype_validation_true()
    {
        $images = new Maksoft\Form\Fields\FileInputField(array(
            "label"=>"DropImages Here",
            "name" => "images[]",
            "id"   => "img",
            "class"=>'form-control',
            "multiple" => True,
            ));
        $images->add_validators(new Maksoft\Form\Validators\FileTypeMatch("image/jpeg"));
        $this->assertTrue($images->is_valid());
    }


    public function test_is_valid_with_no_files_attached()
    {
        $images = new Maksoft\Form\Fields\FileInputField(array(
            "label"=>"DropImages Here",
            "name" => "images",
            "id"   => "img",
            "class"=>'form-control',
            "multiple" => True,
            ));
	$_FILES = array();
        try {
	    $images->add_validators(new Maksoft\Form\Validators\NotBiggerThan(2000000));
            $images->is_valid();
        } catch (Exception $e) {
            $this->assertEquals("no files attached. What to validate?", $e->getMessage());
        }
    }


    /**
     * @covers Maksoft\Form\Fields\FileInputField::is_valid
     */
    public function test_multiple_files_input_filetype_validation_expect_exception()
    {
        $images = new Maksoft\Form\Fields\FileInputField(array(
            "label"=>"DropImages Here",
            "name" => "images[]",
            "id"   => "img",
            "class"=>'form-control',
            "multiple" => True,
            ));
        $validator = new Maksoft\Form\Validators\FileTypeMatch("image/gif");
        $images->add_validators($validator);
        try {
            $images->is_valid();
        } catch (Exception $e) {
            $this->assertEquals(Maksoft\Form\Fields\BaseField::VALIDATOR_FAIL, $e->getCode());
            $this->assertEquals($validator->msg, $e->getMessage());
        }
    }

    /**
     * @covers Maksoft\Form\Fields\FileInputField::is_valid
     */
    public function test_multiple_files_input_filesize_validator_expect_exception()
    {
        $images = new Maksoft\Form\Fields\FileInputField(array(
            "label"=>"DropImages Here",
            "name" => "images[]",
            "id"   => "img",
            "class"=>'form-control',
            "multiple" => True,
            ));
        $validator = new Maksoft\Form\Validators\NotBiggerThan(123);
        $images->add_validators($validator);
        try {
            $images->is_valid();
        } catch (Exception $e) {
            $this->assertEquals(Maksoft\Form\Fields\BaseField::VALIDATOR_FAIL, $e->getCode());
            $this->assertEquals($validator->msg, $e->getMessage());
        }
    }

    /**
     * @covers Maksoft\Form\Fields\FileInputField::is_valid
     */
    public function test_multiple_files_input_filesize_validator_expect_true()
    {
        $images = new Maksoft\Form\Fields\FileInputField(array(
            "label"=>"DropImages Here",
            "name" => "images[]",
            "id"   => "img",
            "class"=>'form-control',
            "multiple" => True,
            ));
        $validator = new Maksoft\Form\Validators\NotBiggerThan(1212333);
        $images->add_validators($validator);
        $this->assertTrue($images->is_valid());
    }

    public function testRunValidators_on_error()
    {
        $this->password->value = 'asda';
        try{
            $this->password->is_valid($this->password->value);
        }catch(Maksoft\Form\Exceptions\ValidationError $e){
            $class = get_class($e);
            $this->assertEquals($class, "Maksoft\Form\Exceptions\ValidationError");
        }
    }

    public function test_check_that_all_validators_added_to_password_field_are_valid()
    {
        $password = new Maksoft\Form\Fields\PasswordField(
                        ["name"=>"password", "label"=>'Парола', "class"=>'form-control']
        );
        $password->add_validators(new Maksoft\Form\Validators\MaxLength(8));
        $password->add_validators(new Maksoft\Form\Validators\MinLength(5));
        $password->add_validators(new Maksoft\Form\Validators\HasDigit());
        $password->add_validators(new Maksoft\Form\Validators\HasUpperCase());
        $password->add_validators(new Maksoft\Form\Validators\HasSpecialChars("?!@#$%^&(*)"));
        $password->value = 'Admn00%';
        $this->assertEquals($password->is_valid(), True);
    }

    public function test_check_that_all()
    {
        try {
            $this->password->value = '.';
            $this->password->is_valid();
        } catch (Exception $e) {
            $this->assertEquals($e->getCode(), 1);
        }
    }

    public function testAddValidators_on_success()
    {
        $this->assertEquals($this->password->add_validators(new Maksoft\Form\Validators\HasSpecialChars('%&%*')), $this->password);
    }

    public function testAddValidators_not_callable_add()
    {
        $dummy_validator = 'some shit that "MUST WORK" :)';
        $code = 1;
        try{
            $this->password->add_validators($dummy_validator);
        } catch (\Maksoft\Form\Exceptions\ValidationError $e){
            $code = $e->getCode();
        }
        $this->assertEquals(100, $code);
    }

    public function test_Form_save_method_with_one_hidden_field()
    {
		$post = array("name" => "adkksakld", "SiteID" => '999');
		$form = new TestForm($post);
		$form->is_valid();
		$this->assertEquals(json_encode($post), $form->save());
    }

    public function test_Form_cleaned_fields_not_exist_extra_information_passed_to_form()
    {
        $form = new TestForm();
        $post = array("name" => "adkksakld", "SiteID" => '999', "csrf" => 'asdasd');
        $form = new TestForm($post);
        $form->is_valid();
        $this->assertNotEquals(json_encode($post), $form->save());
    }

	public function test_email_validator()
	{
		$email = "sales@maksoft.bg";
		$validator = new \Maksoft\Form\Fields\EmailField();
		$validator->value=$email;
		$this->assertTrue($validator->is_valid());
	}

	public function test_form_with_email()
	{
		$post = array("email" => "sales@maksoft.bg", "SiteID" => '999');
		$form = new TestFormEmail(array("SiteID"=>999));
		$form->is_valid();
		$this->assertTrue(boolval($form->is_valid()));
	}

    public function test_validators_integerish_assert_true()
    {
        $validator = new \Maksoft\Form\Validators\Integerish();
        $this->assertTrue($validator("50"));
    }

    public function test_validators_integerish_assert_false()
    {
        $validator = new \Maksoft\Form\Validators\Integerish();
        $this->assertFalse($validator("50sa"));
    }

    public function test_validators_integerish_float__assert_false()
    {
        $validator = new \Maksoft\Form\Validators\Integerish();
        $this->assertFalse($validator("1.123"));
        $this->assertFalse($validator(1.123));
    }

    public function test_DateField_default_validation()
    {
        $date = new \Maksoft\Form\Fields\DateField(array(
            "name" => "to_date",
            "id" => "to_date",
            "value" => "28.02.1988"
        ));

        $date->set_format('Y%m%d');
        $this->assertEquals("1988%02%28", (string) $date->is_valid(),
            "Проверява дали връща валидна дата в предварително  зададен формат"
        );

        $code = 0;

        $date->value = "kljsahsadjfas";
        try{
            $date->is_valid();
        } catch(\Exception $e){
            $code = $e->getCode();
        }
        $this->assertEquals(31, $code,
            "Проверява дали се хвърля ексепшън когато датата е невалидна"
        );

        $date->value = "02.02.jsadhsakj";
        $code = 0;
        try{
            $date->is_valid();
        } catch(\Exception $e){
            $code = $e->getCode();
        }
        $this->assertEquals(31, $code,
            "Проверява дали се хвърля ексепшън когато датата е невалидна"
        );
        
        $date->value = "2016";
        $code = 0;
        $this->assertEquals(date('Y%m%d',time()), (string) $date->is_valid(),
            "При въвеждане само на година трябва да върне 
            днешната дата в предаврително зададеният формат"
        );
    }

    public function test_PhoneField()
    {
        $phone = new \Maksoft\Form\Fields\PhoneField();
        $phone->value= "02/8464646";
        $this->assertTrue($phone->is_valid(), "Валидира номера по регулярен израз и връща True ако е валиден");

        $phone->value= "02/846464s6";
        $code = 0;
        try {
            $phone->is_valid();
            echo $phone->value;
        } catch ( \Exception $e){
            $code = $e->getCode();
        }

        $this->assertEquals(33, $code);   

        $phone->value= "+35902/846464s6";
        $code = 0;
        try {
            $phone->is_valid();
            echo $phone->value;
        } catch ( \Exception $e){
            $code = $e->getCode();
        }

        $this->assertEquals(33, $code);   

        $phone->value= "+35902/8464646";
        $this->assertTrue($phone->is_valid(), "Валидира номера по регулярен израз и връща True ако е валиден");
    }

}
