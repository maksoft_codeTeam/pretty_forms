<?php
include 'vendor/autoload.php';

use Maksoft\Form\Fields\TextField;
use Maksoft\Form\Fields\EmailField;
use Maksoft\Form\Fields\TextAreaField;
use Maksoft\Form\Fields\SubmitButton;
use Maksoft\Form\Exceptions\ValidationError;
use Maksoft\Form\Forms\DivForm;


class ContactForm extends DivForm
{
    public function __construct($form_data=null)
    {
        $this->test = TextField::init()
                ->add('name' , 'test')
                ->add('required' , True)
                ->add("class", "form-group");
        $this->from = TextField::init()
                ->add('label', 'Вашето Име:')
                ->add('class', 'form-control')
                ->add('required', True);

        $this->email = EmailField::init()
                ->add("label", "Email*")
                ->add("name" , "email")
                ->add("class", 'form-control')
                ->add("required",True);

        $this->subject = TextAreaField::init()
                ->add("label", "Запитване")
                ->add("name" , "subject")
                ->add("class", 'form-control')
                ->add("required", True);
        $this->submit = SubmitButton::init()
            ->add("class", "btn btn-default")
            ->add("value", "Изпрати");
        parent::__construct($form_data);
    }
}

$form = new ContactForm();
$form->setId('csv_form');
$form->setAction('/url/goes/here.php');
echo $form;
