<?php
namespace Maksoft\Form\Forms;
use \Maksoft\Form\Fields\CsrfToken;


class Form
{
    public $_attr = array();
    public $_cleaned_data=array();
    private $_fields = array();
    protected $_post = array();

    /**
     * Loops over each element in the defined object variables check if there is some private or protected
     * properties and add all of them in _fields array.
     * Second Loop check if form_data is not null and iterate over it. If form_data[KEY] is equal to
     * some of object variables check if this var is object. If is true add value to object (eg. for validation)
     *
     *
     * @param array $arr
     * @return none
     */

    public function __construct($post_data=null)
    {
        $this->post = $post_data;
        foreach(get_object_vars($this) as $instance=>$value){
            if(is_object($value)){
                $value->name = $instance;
                $this->{$instance} = $value;
                $this->_fields[] = $this->{$instance};
            }
        }
        $this->_attr['action'] = false;
        $this->_attr['name'] = false;
        $this->_attr['enctype'] = "application/x-www-form-urlencoded";
        $this->_attr['method'] = "POST";
        $this->_attr['id'] = false;
    }

    public function start()
    {
        $str = '';
        foreach ($this->_attr as $name=>$value){
            if(!$value){ continue; }
            $str .= sprintf("%s=\"%s\" ", $name, $value);
        }
        return sprintf("<form %s>", $str);
    }

    public function end()
    {
        return "</form>";
    }

    public function fields()
    {
        return $this->_fields;
    }

    protected function clean_request(array $post_data)
    {
        foreach($post_data as $key=>$value){
            if(!array_key_exists($key, get_object_vars($this))){ continue; }
            if(is_object($this->$key) && method_exists($this->$key, 'is_valid')){
                $this->$key->value = $value;
            }
        }
    }

    public function is_valid()
    {
        $this->clean_request($this->post);

        $this->run_validators();

        return $this->_cleaned_data;
    }

    /**
     *Check if every Field in form is valid and if there is custom method of validation check it too
     *
     * Loops over each element in the object attributes and check if is object if True call is_valid() on object
     * for each var try if there is method of validation if is defined such method try to call it.
     *
     *
     * @param null
     * @return array
     * @throws ValidationError inherited from fields classes
     */
    protected function run_validators()
    {
        foreach (get_object_vars($this) as $instance=>$value){
            if(is_object($this->$instance) && method_exists($this->$instance, 'is_valid')){
                $this->$instance->is_valid();
                $validator_name = sprintf("validate_%s", $instance);
                if(method_exists($this, $validator_name)){
                    $this->$validator_name($value);
                }
                $this->_cleaned_data[$instance] = $this->$instance->value;
            }
        }
    }

    /**
     * Return string representation of form
     *
     * @param null
     * @return string
     */
    public function __toString()
    {
        $tmp = '';
        foreach (get_object_vars($this) as $input_field):
            if(is_object($input_field)):
                $tmp.= (string) $input_field;
            endif;
        endforeach;
        return $tmp;
    }

	public function save()
	{
        if(array_key_exists("csrf", $this->_cleaned_data)){
            unset($this->_cleaned_data['csrf']);
        }
        return json_encode($this->_cleaned_data);
	}

    public function setAction($url){
        $this->_attr['action'] = $url;
    }

    public function getAction(){
        if(!$this->_attr['action']){
            return '';
        }
        return $this->_attr['action'];
    }

    public function setMethod($method){
        $this->_attr['method'] = $method;
    }

    public function getmethod(){
        return $this->_attr['method'];
    }

    public function setName($name){
        $this->_attr['name'] = $name;
    }

    public function getName(){
        if(empty($this->_attr['name'])){
            return '';
        }
        return $this->_attr['name'];
    }

    public function setId($id){
        $this->_attr['id'] = $id;
        return $this;
    }

    public function getId()
    {
        return $this->_attr['id'];
    }
    
    public function setClass($class){
        $this->_attr['class'] = $class;
        return $this;
    }

    public function getClass()
    {
        return $this->_attr['class'];
    }

    public function getCleanedData()
    {
        return $this->_cleaned_data;
    }
}
?>
