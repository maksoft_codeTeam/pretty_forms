<?php
namespace Maksoft\Form\Fields;


class DateTimeField extends InputField
{
    public function __construct($kwargs=array()){
        $this->data['type'] = 'datetime';
        parent::__construct($kwargs);
    }

    /**
     * Check if DateInputField field is valid
     * See constructor of Form class second loop -> form_data
     *
     * @param null
     * @return str today date format dd-mm-YYYY
     */
    public function is_valid()
    {
        parent::is_valid();
        $date = new DateTimeField($this->value);
        #return (string) $date;
    }
}

?>
