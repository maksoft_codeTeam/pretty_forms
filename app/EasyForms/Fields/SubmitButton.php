<?php
namespace Maksoft\Form\Fields;


 /**
  * Class SubmitInputField extends from InputField
  *
  *
  * @author  Radoslav Yordanov cc@maksoft.bg>
  *
  * @since 1.0
  */
class SubmitButton extends InputField
{
    public function __construct(array $kwargs=array()){
        $this->data['type'] = 'submit';
        parent::__construct($kwargs);
    }
}

?>
