<?php
namespace Maksoft\Form\Fields;
use Maksoft\Form\Exceptions\ValidationError;

/**
 * This is a summary
 *
 * This is a description
 *
 * @author Radoslav Yordanov
 */
abstract class BaseField implements Field
{
    /**  @var get all defined vars */
    public  $__dict__;

    /**  @var array that store all empty instances */
    private $__empty_values = array();


    /**  @var get all defined vars */
    protected $data = array();

    /**  @var get all defined vars */
    protected $__default_validators = array();

    const VALIDATOR_FAIL = 101;
    const NOT_CALLABLE = 100;
    const SESSION_NOT_START = 102;
    const INVALID_CSRF_TOKEN = 103;
    const INVALID_DATE = 31;
    const EMPTY_FILE = 1233;

    public function __construct(array $kwargs)
    {
        $defined = get_object_vars($this);
        foreach($kwargs as $attribute => $value){
            if(empty($value)){
                $this->__empty_values[] = $attribute;
                continue;
            }
            $this->data[$attribute] = $value;
        }
        return $this;
    }

    public abstract function __toString();

    public function __set($attribute, $value)
    {
        $this->data[$attribute] = $value;
    }

    public function __get($attribute)
    {
        return $this->exist($attribute) ? $this->data[$attribute] : null;
    }

    public function __isset($attribute)
    {
        return isset($this->data[$attribute]);
    }

    public function __unset($attribute)
    {
        unset($this->data[$attribute]);
    }

    public function is_valid(){
        return True;
    }

    public function add($attribute, $value)
    {
        $this->data[$attribute] = $value;
        return $this;
    }

    private function run_validators($valueToValidate){
        $errors = array();

        foreach($this->__default_validators as $validatorInstance){
            try{
                $validatorInstance($valueToValidate);
            }catch (ValidationError $e){
                $e[] = $e->message();
            }
        }
        if($errors){
            throw new ValidationError($errors, self::VALIDATOR_FAIL);
        }
    }

    public function add_validators($validatorInstance)
    {
        if(is_callable($validatorInstance)){
            $this->__default_validators[] = $validatorInstance;
            return $this;
        }
        throw new ValidationError("add_validators method accepts only callables", self::NOT_CALLABLE);
    }

    public function add_validator($validatorInstance)
    {
        if(is_callable($validatorInstance)){
            $this->__default_validators[] = $validatorInstance;
            return $this;
        }
        throw new ValidationError("add_validator method accepts only callables", self::NOT_CALLABLE);
    }

    protected function createFieldAttr()
    {
        $element = "";
        foreach ($this->data as $htmlAttribute => $value){
            if(is_array($value)){ continue; }
            if(is_bool($value) && $value){
                $element .= ' '.$htmlAttribute.' ';
                continue;
            }
            if(!is_null($value) && $value && $value !== '_'){
                $element .= $htmlAttribute.'="'.$value.'" ';
            }
        }
        return $element;
    }

   public function has_changed($data, $initial){
        /*
         * Return True if data differs from initial
         */
        try{
            if(in_array('_coerce', get_class_methods($this)))
                return ($this->_coerce($data) != $this->_coerce($initial)) ? True : False;
        }catch (ValidationError $e){
            return True;
        }
        /*
         * For purposes of seeing weather something has changed, None is the same
         * as an empty string, if the data or initial value we get is None, replace with '',
         */
        $initial_value = ($initial == null) ? '' : $initial;
        $data_value = ($data == null) ? '' : $data;

        return ($initial_value != $data_value) ? True : False;
    }

    public function exist($attribute)
    {
        return array_key_exists($attribute, $this->data);
    }

    public function getLabel()
    {
        if($this->exist('label')){
            return sprintf("<label for\"%s\">%s</label>", $this->data['name'], $this->data['label']);
        }
        return "";
    }

    public static function init(array $kwargs=array())
    {
        $className = get_called_class();
        return new $className($kwargs);
    }
}

?>

