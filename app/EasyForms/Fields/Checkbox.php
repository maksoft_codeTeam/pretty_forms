<?php
namespace Maksoft\Form\Fields;


 /**
  * Class TextInputField extends from InputField
  *
  * @param  this is type of the input field'
  *
  * @author  Radoslav Yordanov cc@maksoft.bg>
  *
  * @since 1.0
  */
class Checkbox extends InputField
{
    public function __construct(array $kwargs=array())
    {
        $this->data['type'] = 'checkbox';
        parent::__construct($kwargs);
    }
}
