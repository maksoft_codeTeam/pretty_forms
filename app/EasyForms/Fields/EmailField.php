<?php
namespace Maksoft\Form\Fields;
use Maksoft\Form\Validators\EmailValidator;


class EmailField extends InputField
{
    public function __construct($kwargs=array())
    {
        $this->data['type'] = 'email';
        $this->add_validator(EmailValidator::init());
        parent::__construct($kwargs);
    }
}

?>
