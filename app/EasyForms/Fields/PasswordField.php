<?php
namespace Maksoft\Form\Fields;


 /**
  * Class PasswordInputField extends from InputField
  *
  *
  * @author  Radoslav Yordanov cc@maksoft.bg>
  *
  * @since 1.0
  */
class PasswordField extends InputField
{
    public function __construct(array $kwargs=array()){
        $this->data['type'] = 'password';
        parent::__construct($kwargs);
    }
}

?>
