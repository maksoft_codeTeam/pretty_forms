<?php
namespace Maksoft\Form\Fields;


class DateField extends InputField
{
    protected $format = "Y-m-d";
    public function __construct($kwargs=array()){
        $this->data['type'] = 'date';
        parent::__construct($kwargs);
    }

    /**
     * Check if DateInputField field is valid
     * See constructor of Form class second loop -> form_data
     *
     * @param null
     * @return str today date format dd-mm-YYYY
     */
    public function is_valid()
    {
        parent::is_valid();
        try{
            $date = new \DateTime($this->value);
            $this->value = $date->format($this->format);
            return $this->value;
        } catch (\Exception $e){
            throw new \Exception(sprintf("Invalid Date[\"%s\"] provided", $this->value), self::INVALID_DATE);
        }
    }

    public function set_format($date_format){
        $this->format = $date_format;
    }
}

?>
