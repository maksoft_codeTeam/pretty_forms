<?php
namespace Maksoft\Form\Fields;
use Maksoft\Form\Exceptions\ValidationError;


class InputField extends BaseField
{
    private $type="input";

    public function __construct(array $kwargs=array()){
        parent::__construct($kwargs);
    }

    /**
     * Check if given field is valid
     *
     * Loops over each element in the validators array which
     * is callable objects and check state for True
     *
     *
     * @param null
     * @return null
     * @throws ValidationError if element in array return False or throw ValidationError
     */

    public function is_valid(){
        foreach ($this->__default_validators as $validator){
            if($validator($this->value)){ continue; }
            throw new ValidationError($validator->msg, 1);
        }
        return True;
    }

    public function __toString()
    {
        return "    <".$this->type." ".$this->createFieldAttr().'>'.PHP_EOL;
    }
}

?>
