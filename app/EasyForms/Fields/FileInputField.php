<?php
namespace Maksoft\Form\Fields;


 /**
  * Class FileInputField extends from InputField
  *
  *
  * @author  Radoslav Yordanov cc@maksoft.bg>
  *
  * @since 1.0
  */
class FileInputField extends InputField
{
    public function __construct(array $kwargs=array()){
        $this->data['type'] = 'file';
        parent::__construct($kwargs);
    }

    public function is_valid()
    {
        if(in_array('required', $this->data)){
            if(empty($_FILES)){
                throw new \Exception("no files attached. What to validate?", self::EMPTY_FILE);
            }
        }


        $name = explode("[]", $this->data['name']);
        $name = $name[0];
        foreach ($this->__default_validators as $validator){
            if(isset($this->data['multiple'])){
                $files = $this->reArrayFiles($_FILES[$name]);
                foreach($files as $file){
                    $this->validate($validator, $file);
                }
            }else{
                $this->validate($validator, $_FILES[$name]);
            }
        }
        return True;
    }

    private function validate($validator, $file)
    {
        if($validator($file)){ return; }
        throw new \Exception($validator->msg, self::VALIDATOR_FAIL);
    }


    public function reArrayFiles(&$file_post)
    {
        $file_ary = array();
        $file_count = count($file_post['name']);
        $file_keys = array_keys($file_post);

        for ($i=0; $i<$file_count; $i++) {
            foreach ($file_keys as $key) {
                $file_ary[$i][$key] = $file_post[$key][$i];
            }
        }
        return $file_ary;
    }
}

?>
