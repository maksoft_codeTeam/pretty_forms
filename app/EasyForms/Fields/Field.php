<?php
namespace Maksoft\Form\Fields;


interface Field
{
    public function __toString();

    public function is_valid();

    public function add_validator($validator);

    public function add_validators($validator);

    public static function init();
}
