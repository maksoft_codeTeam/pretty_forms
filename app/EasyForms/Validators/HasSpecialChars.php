<?php
namespace Maksoft\Form\Validators;


class HasSpecialChars extends Base
{
     public function __construct($allowed_chars)
    {
        $this->allowed_chars = $allowed_chars;
        $this->msg = sprintf("���� ��������� ����[%s]", $allowed_chars);
    }

    public function __invoke($password)
    {
        foreach (str_split($this->allowed_chars) as $spec_char):
            if(strpos($password, $spec_char))
                return True;
        endforeach;
        return False;
    }
}

?>
