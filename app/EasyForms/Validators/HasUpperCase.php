<?php
namespace Maksoft\Form\Validators;


class HasUpperCase extends Base
{
    public function __construct($default=True)
    {
        $this->default = $default;
        $this->msg = "�� ������� ������ �����!";
    }

    public function __invoke($password)
    {
        foreach (str_split($password) as $char) {
            if(ctype_upper($char))
                return True;
        }
        return False;
    }
}

?>
