<?php
namespace Maksoft\Form\Validators;


class MaxLength extends Base
{
    public function __construct($length)
    {
        $this->length = $length;
        $this->msg = "��������� �������! ������������ ��������� ������� � ".$length;
    }

    public function __invoke($password)
    {
        return strlen($password) <= $this->length;
    }
}

?>
