<?php
namespace Maksoft\Form\Validators;


class Integerish extends Base
{
	public function __construct($args=null, $kwargs=null){
		$this->msg = "O������� �������� - �����. ��������: %s";
	}

	public function __invoke($value=null){
        if (!is_numeric($value) || $value != (int) $value) {
            $this->msg = sprintf($this->msg, $value);
			return false;
        }
		return true;
	}
}
