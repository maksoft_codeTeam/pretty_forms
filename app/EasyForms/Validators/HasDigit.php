<?php
namespace Maksoft\Form\Validators;


class HasDigit extends Base
{
    public function __construct($default=True)
    {
        $this->default = True;
        $this->msg = "������������ ������ �� ��� �����";
    }

    public function __invoke($password)
    {
        foreach (str_split($password) as $char) {
            if(is_numeric($char))
                return True;
        }
        return False;
    }
}

?>
