<?php
namespace Maksoft\Form\Validators;


class MinLength extends Base
{
    public function __construct($length)
    {
        $this->length = $length;
        $this->msg = sprintf("Невалидна минимална дължина [%s] символа.",$length);
    }

    public function __invoke($value)
    {
        return strlen($value) > $this->length;
    }
}

?>
