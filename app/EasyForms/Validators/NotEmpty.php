<?php
namespace Maksoft\Form\Validators;


class NotEmpty extends Base
{
    public function __construct($default=True)
    {
        $this->not_empty = $default;
        $this->msg = "���� ���� �� ���� �� ���� ������!";
    }

    public function __invoke($value=null)
    {
        if(empty($value) or is_null($value) or strlen($value) == 0)
            return !$this->not_empty;
        return $this->not_empty;
    }
}

?>
