<?php
namespace Maksoft\Form\Validators;


class FileExtensionMatch extends Base
{
	public $extensions = array();
    public function __construct($extensions)
    {
		foreach(func_get_args() as $extension){
			$this->extensions[] = strtolower(str_replace(".", "", $extension));
		}
        $this->msg = "invalid extension"; 
    }

    public function __invoke($file=array())
    {
		$file_ext = explode(".", $file["name"]);
		$file_ext = str_replace(".", "", end($file_ext));
		
		foreach($this->extensions as $extension){
			if($extension == $file_ext){
				return True;
			}
		}
		return False;
    }
}

?>
