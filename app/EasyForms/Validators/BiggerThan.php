<?php
namespace Maksoft\Form\Validators;


class BiggerThan extends Base
{
    public function __construct($size)
    {
        $this->size = $size;
        $this->msg = sprintf("������ � ��-����� �� ���������� ����������� ������ - %s", $this->format_size($size)); 
    }

    public function __invoke($file=array())
    {
        if(!array_key_exists('size', $file)){ return false; }
        return $file['size'] > $this->size;
    }
}

?>
