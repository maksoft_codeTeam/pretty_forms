<?php
namespace Maksoft\Form\Validators;


class EmailValidator extends Base
{
	public function  __invoke($email)
	{
		$this->msg = sprintf("����� �������, ����� ��� ������������(%s) � ���������.", $email);	
		return filter_var($email, FILTER_VALIDATE_EMAIL);
	}
}
